rm -rf out
mkdir -p out
docker run --rm -it --user "$(id -u):$(id -g)" -v .:/opt/texlive registry.gitlab.com/googol_r/container-images/texlive pdflatex -interaction nonstopmode -halt-on-error -output-directory out -jobname=cv cv.tex
docker run --rm -it --user "$(id -u):$(id -g)" -v .:/opt/texlive registry.gitlab.com/googol_r/container-images/texlive pdflatex -interaction nonstopmode -halt-on-error -output-directory out -jobname=cven cv.tex
