# cv
My resume.

# Pdf
You can download the cv in pdf format in the releases. These links point to the latest versions:
  - [Finnish](https://gitlab.com/googol_r/cv/-/releases/permalink/latest/downloads/cv.pdf)
  - [English](https://gitlab.com/googol_r/cv/-/releases/permalink/latest/downloads/cven.pdf)
